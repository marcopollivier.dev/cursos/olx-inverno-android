package br.com.ollivier.olxbrasil

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val meuBotao = findViewById<Button>(R.id.botao)

        meuBotao?.setOnClickListener {
            Log.i("Ciclo de Vida -> evento", "onClick botao")

            val textoDigitado = campo.text.toString()

            Log.i("Ciclo de Vida -> valor digitado", textoDigitado)
            label.text = textoDigitado

            Log.i("Ciclo de Vida -> envia para intent", textoDigitado)
            val intent = Intent(this@MainActivity, BrandListActivity :: class.java)
            intent.putExtra("texto", textoDigitado)
            startActivity(intent)
        }

        Log.i("Ciclo de Vida", "onCreate()")
    }

    override fun onStart() {
        super.onStart()

        Log.i("Ciclo de vida", "onStart()")
    }

    override fun onResume() {
        super.onResume()

        Log.i("Ciclo de vida", "onResume()")
    }

    override fun onPause() {
        super.onPause()

        Log.i("Ciclo de vida", "onPause()")
    }

    override fun onStop() {
        super.onStop()

        Log.i("Ciclo de vida", "onStop()")
    }

    override fun onRestart() {
        super.onRestart()

        Log.i("Ciclo de vida", "onRestart()")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.i("Ciclo de vida", "onDestroy()")

    }

}
