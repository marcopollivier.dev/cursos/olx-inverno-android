package br.com.ollivier.olxbrasil

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_brand_list.*
import java.lang.ref.WeakReference

class BrandListActivity : AppCompatActivity() {

    private lateinit var task: BrandTask
    private lateinit var adapter: ArrayAdapter<Brand>
    private lateinit var brands: MutableList<Brand>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brand_list)

        Log.i("Ciclo BrandList", "onCreate()")

        brands = mutableListOf(Brand(1, "Audi"), Brand(2, "BMW"), Brand(3, "GM"))
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, brands)
        listBrands.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        task = BrandTask(WeakReference(this))
        task.execute()
    }

    override fun onDestroy() {
        super.onDestroy()
        task.cancel(true)
    }

    fun showBrands(brands: List<Brand>) {
        this.brands.clear()
        this.brands.addAll(brands)
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.add -> {
                Log.i("Click", "Add Menu")

                val intent = Intent(this@BrandListActivity, FormActivity :: class.java)
                //intent.putExtra("texto", textoDigitado)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
